FROM python:3.6

ENV LC_ALL C.UTF-8

WORKDIR /app

EXPOSE 8000

COPY requirements.txt /app/

RUN pip install -r /app/requirements.txt

COPY . /app

CMD ["python3", "manage.py", "runserver", "0:8000"]
