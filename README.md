# GitLab CI練習プロジェクト

[Djangoチュートリアル](https://docs.djangoproject.com/ja/2.2/intro/tutorial01/)のアプリでGitLab CIの基礎を学びましょう。

## 必要環境

- Docker

## 環境構築

1. 本リポジトリをクローン
2. クローンしたディスカウントに移動
3. Dockerイメージをビルド
    ```
    docker build -t django-sample .
    ```
4. コンテナを起動
    ```
    docker run --rm -v $PWD:/app -p 8000:8000 --name django-sample django-sample
    ```
5. DBのマイグレーション
    ```
    docker exec django-sample python manage.py migrate
    ```
6. 初期データを作成
    ```
    docker exec -it django-sample python manage.py loaddata questions.yaml
    ```
7. http://localhost:8000/polls にアクセスする
